<?php 
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {

 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Change Password</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
<header>
        <nav>
            <a href="./index.php"><h1 id="navtext">PLATFORMERI</h1></a>
            <ul>
                <li><a href="" class="nykyinensivu">Change personal information</a></li>
				<li><a href="./profile.php">Profile</a></li>
                <li><a href="./php/logout.php">logout</a>
            </ul>
        </nav>
    </header>
    <div class="kirjaudu_box">
	<section id="profileSection">
        <div class="profile-info">
		<form action="" method="post">
    <h2>Change Information</h2>
    <?php if (isset($_GET['error'])) { ?>
        <p class="error"><?php echo $_GET['error']; ?></p>
    <?php } ?>

    <?php if (isset($_GET['success'])) { ?>
        <p class="success"><?php echo $_GET['success']; ?></p>
    <?php } ?>

    <h3>Change Password</h3>
    <label>Old Password:</label>
    <input type="password" name="op" placeholder="Old Password"><br>
    <label>New Password:</label>
    <input type="password" name="np" placeholder="New Password"><br>
    <label>Confirm New Password:</label>
    <input type="password" name="c_np" placeholder="Confirm New Password"><br>

    <h3>Change Username</h3>
    <input type="text" name="new_username" placeholder="New Username"><br>

    <h3>Change Email</h3>
    <input type="email" name="new_email" placeholder="New Email"><br>

    <input type="password" name="current_password" placeholder="Current Password for Verification"><br>
    <button type="submit">Submit Changes</button>
</form>

		</div>
	</section>
    </div>
</body>
</html>

<?php 
}else{
     header("Location: index.php");
     exit();
}
 ?>