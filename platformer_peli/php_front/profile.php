<?php session_start();

if (isset($_SESSION['id']) && isset($_SESSION['user_name']) && isset($_SESSION['email'])) {
    $email = $_SESSION['email']; 
    include "../php_back/db_conn.php";
?>

<!DOCTYPE html>
<html lang="fi">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../css/style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
</head>
<body>
    <header>
        <nav>
            <a href=""><h1 id="navtext">Profiili</h1></a>
            <ul>
                <li><a href="../html/etusivu.html" class="nykyinensivu">Etusivu</a></li>
                <li><a href="change-password.php">Change personal information</a></li>
                <li><a href="../php_back/logout.php">Kirjaudu ulos</a>
            </ul>
        </nav>
    </header>
    <div class="kirjaudu_box">
    <section id="profileSection">
        <div class="profile-info">
            <h2><?php echo $_SESSION['user_name']; ?></h2>
            <?php if (isset($_GET['error'])): ?>
                <p><?php echo $_GET['error']; ?></p>
            <?php endif ?>

            <div id="imageUploadForm" style="display: none;">
                <form action="../php_back/upload.php" method="post" enctype="multipart/form-data">
                    <input type="file" name="my_image">
                    <input type="submit" name="submit" value="Upload">
                </form> 
            </div>

            <?php 
        // Prepare the SQL statement with a placeholder for the email
        $stmt = $conn->prepare("SELECT * FROM users WHERE email = ? ORDER BY id DESC");

        // Bind the $email variable to the placeholder
        $stmt->bind_param("s", $email);

        // Execute the prepared statement
        $stmt->execute();

        // Get the result of the query
        $res = $stmt->get_result();

        if ($res->num_rows > 0) {
            while ($users = $res->fetch_assoc()) {
                // Display the user's information, for example:
                echo '<div class="alb">';
                echo '<img src="../uploads/' . htmlspecialchars($users['image_url']) . '" alt="User Image" class="profile-pic">';
                echo '</div>';
                
            }
        } else {
            echo 'No user information found.';
        }

        // Close the prepared statement
        $stmt->close();
    ?>
                <p><?php echo $_SESSION['user_name']; ?></p>
                <p><?php echo $_SESSION['email'] ?></p>


            <div class="buttons-container">
                <button id="editProfileButton">Muokkaa profiilia</button>
                <button id="saveChangesButton" style="display: none;">Tallenna muutokset</button>
            </div>
        </div>
    </section>
    </div>
    <footer>

    </footer>

    <script src="../javascript/profile.js"></script>
</body>
</html>

<?php
} else {
    header("location: profile.php");
    exit();
}
?>
