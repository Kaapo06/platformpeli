<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./style.css">
    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    <script src="./script.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PLATFORMER PELI NIMI TARVITAAN</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>
    <header>
        <nav>
            <a href="./index.html"><h1 id="navtext">PLATFORMER</h1></a>
            <ul>
                <li><a href="" class="nykyinensivu">Login</a></li>
            </ul>
        </nav>
    </header>

<section>
<div class="kirjaudu_box">

<h2>Login</h2>
<form id="loginform" action="../php_back/login.php" method="post">
    <?php if (isset($_GET['error'])) { ?>
    <p class="error"><?php echo $_GET['error']; ?></p> 
    <?php } ?>
        
        <div class="form-group">
        <div class="form-floating">
            <input type="email" id="email" name="email" class="form-control" placeholder="Email Address" required>
            <label for="email">Email Address</label>
        </div>
        <br>
        <div class="form-floating">
            <input type="password" id="pswd" name="pswd" class="form-control" placeholder="Password" required>
            <label for="pswd">Password</label>
        </div>

        </div>
        <br> 
    <div class="input-group">
    <button type="submit" class="form-control submitbtn" id="loginbtn">Login</button>
    </div>
    <p class="loginp">Don't have an account yet? <a href="luoAcc.php" class="link">Register</a></p>
</form>
</div>
</section>

<footer>

</footer>
</body>
</html>






