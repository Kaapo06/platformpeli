<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PLATFORMERI</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    
    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="../css/style.css">
</head>
<body> 
    <header>
        <nav>
            <a href="./index.html"><h1 id="navtext">PLATFORMER</h1></a>
            <ul>
                <li><a href="" class="nykyinensivu">Login</a></li>
            </ul>
        </nav>
    </header>
    <section id="profileSection">
    <div class="kirjaudu_box">
        <div class="registerform">
        <form action="../php_back/signup-check.php" method="post">
            <h2>SIGN UP</h2>
            <?php if (isset($_GET['error'])) { ?>
                <p class="error"><?php echo $_GET['error']; ?></p>
            <?php } ?>

            <?php if (isset($_GET['success'])) { ?>
                <p class="success"><?php echo $_GET['success']; ?></p>
            <?php } ?>

            <label class="registerlabel">User Name</label>
            <?php if (isset($_GET['uname'])) { ?>
                <input type="text" name="uname" placeholder="User Name" value="<?php echo $_GET['uname']; ?>"><br>
            <?php }else{ ?>
                <input type="text" name="uname" placeholder="User Name" class="registerinput"><br>
            <?php } ?>
            <label class="registerlabel">Email</label>
            <?php if (isset($_GET['email'])) { ?>
                <input type="email" name="email" placeholder="Email" value="<?php echo $_GET['email']; ?>"><br>
            <?php }else{ ?>
                <input type="text" name="email" placeholder="Email" class="registerinput"><br>
            <?php } ?>

            <label class="registerlabel">Password</label>
            <input type="password" name="password" placeholder="Password" class="registerinput"><br>
            
            <label class="registerlabel">Re-type</label>
            <input type="password" name="re_password" placeholder="Re-type pswd" class="registerinput"><br>

            <button type="submit" class="submitbtn">Sign Up</button>
            <br>
            <a href="kirjaudu.php" class="link">Already have an account?</a>
        </form>
        </div>
        </div>
    </section>

    <footer>

    </footer>
</body>
</html>
