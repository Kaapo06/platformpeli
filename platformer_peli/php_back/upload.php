<?php 

session_start(); // Ensure session is started

if (isset($_POST['submit']) && isset($_FILES['my_image']) && isset($_SESSION['id'])) {
    include "db_conn.php";

    $user_id = $_SESSION['id']; // Retrieve the logged-in user's ID

    $img_name = $_FILES['my_image']['name'];
    $img_size = $_FILES['my_image']['size'];
    $tmp_name = $_FILES['my_image']['tmp_name'];
    $error = $_FILES['my_image']['error'];

    if ($error === 0) {
        if ($img_size > 12500000) {
            $em = "Sorry, your file is too large.";
            header("Location: ../php_front/profile.php?error=$em");
            exit();
        } else {
            $img_ex = pathinfo($img_name, PATHINFO_EXTENSION);
            $img_ex_lc = strtolower($img_ex);

            $allowed_exs = array("jpg", "jpeg", "png"); 

            if (in_array($img_ex_lc, $allowed_exs)) {
                $new_img_name = uniqid("IMG-", true).'.'.$img_ex_lc;
                $img_upload_path = '../uploads/'.$new_img_name;
                move_uploaded_file($tmp_name, $img_upload_path);

                // Update the database record for only the logged-in user
                $sql = "UPDATE users SET image_url = ? WHERE id = ?";
                $stmt = mysqli_prepare($conn, $sql);
                if (!$stmt) {
                    $em = "SQL error occurred!";
                    header("Location: ../php_front/profile.php?error=$em");
                    exit();
                }

                mysqli_stmt_bind_param($stmt, "si", $new_img_name, $user_id);
                $result = mysqli_stmt_execute($stmt);

                if ($result) {
                    header("Location: ../php_front/profile.php");
                } else {
                    $em = "Error updating profile image.";
                    header("Location: ../php_front/profile.php?error=$em");
                }

                mysqli_stmt_close($stmt);
            } else {
                $em = "You can't upload files of this type";
                header("Location: ../php_front/profile.php?error=$em");
            }
        }
    } else {
        $em = "unknown error occurred!";
        header("Location: ../php_front/profile.php?error=$em");
    }
} else {
    header("Location: ../php_front/profile.php");
}
?>
