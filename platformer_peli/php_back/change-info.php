<?php
session_start();
if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {
    include "db_conn.php";

    function validate($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    $id = $_SESSION['id'];
    $current_password = validate($_POST['current_password'] ?? '');

    // Initialize an array to keep track of errors
    $errors = [];

    // Verify current password first
    $sql = "SELECT password FROM users WHERE id='$id'";
    $result = mysqli_query($conn, $sql);
    if ($row = mysqli_fetch_assoc($result)) {
        if ($current_password !== $row['password']) {
            $errors[] = "Incorrect current password.";
        }
    } else {
        $errors[] = "Failed to retrieve user data.";
    }

    if (empty($errors)) {
        // Process username change
        if (!empty($_POST['new_username'])) {
            $new_username = validate($_POST['new_username']);
            // Add checks for new username availability, etc.
            $sql = "UPDATE users SET user_name='$new_username' WHERE id='$id'";
            if (mysqli_query($conn, $sql)) {
                $_SESSION['user_name'] = $new_username; // Update session variable
            } else {
                $errors[] = "Failed to update username.";
            }
        }

        // Process email change
        if (!empty($_POST['new_email'])) {
            $new_email = validate($_POST['new_email']);
            // Add checks for new email availability, etc.
            $sql = "UPDATE users SET email='$new_email' WHERE id='$id'";
            if (!mysqli_query($conn, $sql)) {
                $errors[] = "Failed to update email.";
            }
        }

        // Process password change
        if (!empty($_POST['op']) && !empty($_POST['np']) && !empty($_POST['c_np'])) {
            $op = validate($_POST['op']);
            $np = validate($_POST['np']);
            $c_np = validate($_POST['c_np']);
            if ($np !== $c_np) {
                $errors[] = "New password and confirmation password do not match.";
            } elseif ($op === $row['password']) { // Assuming direct password comparison for simplicity, consider using password hashing
                $sql = "UPDATE users SET password='$np' WHERE id='$id'";
                if (!mysqli_query($conn, $sql)) {
                    $errors[] = "Failed to update password.";
                }
            } else {
                $errors[] = "Old password is incorrect.";
            }
        }
    }

    if (!empty($errors)) {
        // Redirect back with the first error
        header("Location: ../php_front/change-password.php?error=" . urlencode($errors[0]));
        exit();
    } else {
        // Redirect back with success message
        header("Location: ../php_front/change-password.php?success=Your information has been updated successfully");
        exit();
    }
} else {
    header("Location: ../php_front/profile.php");
    exit();
}
?>
