<?php 
session_start(); 
include "db_conn.php";
ini_set('display_errors', 1);
error_reporting(E_ALL);





if (isset($_POST['uname']) && isset($_POST['password']) && isset($_POST['re_password']) && isset($_POST['email'])) {

	function validate($data){
       $data = trim($data);
	   $data = stripslashes($data);
	   $data = htmlspecialchars($data);
	   return $data;
	}

	$uname = validate($_POST['uname']);
	$pass = validate($_POST['password']); // Storing password as plain text (not secure)
	$re_pass = validate($_POST['re_password']);
	$email = validate($_POST['email']);

	$user_data = 'uname='. $uname. '&email='. $email;

	if (empty($uname)) {
		header("Location: ../php_front/luoAcc.php?error=User Name is required&$user_data");
	    exit();
	} else if(empty($pass)) {
        header("Location: ../php_front/luoAcc.php?error=Password is required&$user_data");
	    exit();
	} else if(empty($re_pass)) {
        header("Location: ../php_front/luoAcc.php?error=Re Password is required&$user_data");
	    exit();
	} else if(empty($email)) {
        header("Location: ../php_front/luoAcc.php?error=Email is required&$user_data");
	    exit();
	} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header("Location: ../php_front/luoAcc.php?error=Invalid email format&$user_data");
	    exit();
	} else if($pass !== $re_pass) {
        header("Location: ../php_front/luoAcc.php?error=The confirmation password does not match&$user_data");
	    exit();
	} else {
	    $sql = "SELECT * FROM users WHERE user_name='$uname' OR email='$email'";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0) {
			header("Location: ../php_front/luoAcc.php?error=The username or email is already taken, try another&$user_data");
	        exit();
		} else {
			$sql2 = "INSERT INTO users(user_name, password, email) VALUES('$uname', '$pass', '$email')";
			$result2 = mysqli_query($conn, $sql2);
			if ($result2) {
				header("Location: ../php_front/luoAcc.php?success=Your account has been created successfully");
				exit();
			} else {
				// Output MySQL error
				$error = mysqli_error($conn);
				header("Location: ../php_front/luoAcc.php?error=unknown error occurred - $error&$user_data");
				exit();
			}
			
		}
	}
	
} else {
	header("Location: ../php_front/luoAcc.php");
	exit();
}
?>
