document.addEventListener("DOMContentLoaded", function() {
    const editProfileButton = document.getElementById("editProfileButton");
    const imageUploadForm = document.getElementById("imageUploadForm");

    // Ensure elements exist before adding event listeners
    if (editProfileButton && imageUploadForm) {
        editProfileButton.addEventListener("click", function() {
            // Toggle visibility for imageUploadForm
            imageUploadForm.style.display = imageUploadForm.style.display === "block" ? "none" : "block";

            // Optionally, focus the first input in the form to prompt the user
            if (imageUploadForm.style.display === "block") {
                const firstInput = imageUploadForm.querySelector('input[type="file"]');
                if (firstInput) {
                    firstInput.focus();
                }
            }
        });
    }
});

const profileImageInput = document.getElementById("my_image");
const profileImage = document.getElementById("profileImage");

if (profileImageInput && profileImage) {
    profileImageInput.addEventListener("change", function(event) {
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function(e) {
                profileImage.src = e.target.result;
            };
            reader.readAsDataURL(file);
        } else {
            profileImage.src = "../uploads/pfp.png";
        }
    });
}